class CreateUsuariosCategoriasDeServicos < ActiveRecord::Migration
  def change
    create_table :categoria_de_servicos_usuarios do |t|
    	t.integer :usuario_id
      	t.integer :categoria_de_servico_id
      	t.timestamps
    end
  end
end
