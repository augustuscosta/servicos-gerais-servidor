class AddAvatarColumnsToUsuarios < ActiveRecord::Migration
 def self.up
    add_attachment :usuarios, :avatar
  end

  def self.down
    remove_attachment :usuarios, :avatar
  end
end
