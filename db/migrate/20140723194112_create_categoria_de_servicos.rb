class CreateCategoriaDeServicos < ActiveRecord::Migration
  def change
    create_table :categoria_de_servicos do |t|
      t.string :titulo

      t.timestamps
    end
  end
end
