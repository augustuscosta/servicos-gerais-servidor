class CreateUsuarios < ActiveRecord::Migration
  def change
    create_table :usuarios do |t|
      t.string :nome
      t.string :email
      t.string :password
      t.string :password_digest
      t.string :telefone
      t.string :facebook_avatar_url

      t.timestamps
    end
  end
end
