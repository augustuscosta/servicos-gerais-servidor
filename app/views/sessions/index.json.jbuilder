json.array!(@sessions) do |session|
  json.extract! session, :id, :email, :password
  json.url session_url(session, format: :json)
end
