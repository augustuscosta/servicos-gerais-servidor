json.array!(@categoria_de_servicos) do |categoria_de_servico|
  json.extract! categoria_de_servico, :id, :titulo
  json.url categoria_de_servico_url(categoria_de_servico, format: :json)
end
