json.array!(@usuarios) do |usuario|
  json.extract! usuario, :id, :nome, :email, :telefone, :categoria_de_servicos, :avatar_url, :avatar_url_medium ,:avatar_url_thumb ,:facebook_avatar_url ,:created_at, :updated_at
  json.url usuario_url(usuario, format: :json)
end
