class Usuario < ActiveRecord::Base
	has_and_belongs_to_many  :categoria_de_servicos
	has_attached_file :avatar, :styles => { :medium => "300x300>", :thumb => "100x100>" }, :default_url => "/images/:style/missing.png"
	validates_attachment_content_type :avatar, :content_type => ["image/jpg", "image/jpeg", "image/png"]
	has_secure_password
	validates_presence_of :password, :on => :create
	validates_uniqueness_of :email

	attr_accessor :password_confirmation

	def avatar_url_thumb
    	avatar.url(:thumb)
	end

	def avatar_url_medium
    	avatar.url(:medium)
	end

	def avatar_url
    	avatar.url
	end

end
