class CategoriaDeServicosController < ApplicationController
  before_action :set_categoria_de_servico, only: [:show, :edit, :update, :destroy]

  # GET /categoria_de_servicos
  # GET /categoria_de_servicos.json
  def index
    @categoria_de_servicos = CategoriaDeServico.all
  end

  # GET /categoria_de_servicos/1
  # GET /categoria_de_servicos/1.json
  def show
  end

  # GET /categoria_de_servicos/new
  def new
    @categoria_de_servico = CategoriaDeServico.new
  end

  # GET /categoria_de_servicos/1/edit
  def edit
  end

  # POST /categoria_de_servicos
  # POST /categoria_de_servicos.json
  def create
    @categoria_de_servico = CategoriaDeServico.new(categoria_de_servico_params)

    respond_to do |format|
      if @categoria_de_servico.save
        format.html { redirect_to @categoria_de_servico, notice: 'Categoria de servico was successfully created.' }
        format.json { render :show, status: :created, location: @categoria_de_servico }
      else
        format.html { render :new }
        format.json { render json: @categoria_de_servico.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /categoria_de_servicos/1
  # PATCH/PUT /categoria_de_servicos/1.json
  def update
    respond_to do |format|
      if @categoria_de_servico.update(categoria_de_servico_params)
        format.html { redirect_to @categoria_de_servico, notice: 'Categoria de servico was successfully updated.' }
        format.json { render :show, status: :ok, location: @categoria_de_servico }
      else
        format.html { render :edit }
        format.json { render json: @categoria_de_servico.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /categoria_de_servicos/1
  # DELETE /categoria_de_servicos/1.json
  def destroy
    @categoria_de_servico.destroy
    respond_to do |format|
      format.html { redirect_to categoria_de_servicos_url, notice: 'Categoria de servico was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_categoria_de_servico
      @categoria_de_servico = CategoriaDeServico.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def categoria_de_servico_params
      params.require(:categoria_de_servico).permit(:titulo)
    end
end
