class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  # protect_from_forgery with: :exception
  # protect_from_forgery with: :null_session, if: Proc.new { |c| c.request.format == 'application/json' }
  helper_method :current_user

  def authorize
      if session[:usuario_id].nil?
      	respond_to do |format|
        	format.html {
          		flash[:notice] = "Acesso negado!"
          		render :file => "public/404.html", :status => :unauthorized
        	}
        	format.json { render :json => { :error => "Você deve fazer login para utilizar o sistema" }, :status => 401 }
      	end
        
      end

  end

  def current_user
    @current_user ||= Usuario.find(session[:usuario_id]) if session[:usuario_id]
  end

end
