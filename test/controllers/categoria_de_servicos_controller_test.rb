require 'test_helper'

class CategoriaDeServicosControllerTest < ActionController::TestCase
  setup do
    @categoria_de_servico = categoria_de_servicos(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:categoria_de_servicos)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create categoria_de_servico" do
    assert_difference('CategoriaDeServico.count') do
      post :create, categoria_de_servico: { titulo: @categoria_de_servico.titulo }
    end

    assert_redirected_to categoria_de_servico_path(assigns(:categoria_de_servico))
  end

  test "should show categoria_de_servico" do
    get :show, id: @categoria_de_servico
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @categoria_de_servico
    assert_response :success
  end

  test "should update categoria_de_servico" do
    patch :update, id: @categoria_de_servico, categoria_de_servico: { titulo: @categoria_de_servico.titulo }
    assert_redirected_to categoria_de_servico_path(assigns(:categoria_de_servico))
  end

  test "should destroy categoria_de_servico" do
    assert_difference('CategoriaDeServico.count', -1) do
      delete :destroy, id: @categoria_de_servico
    end

    assert_redirected_to categoria_de_servicos_path
  end
end
